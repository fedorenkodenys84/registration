import './App.css';
import { LoginForm } from './components/LoginForm';
import { Header } from './components/Header';

function App() {
  return (
    <main className='h-screen'>
      <Header />
      <div className='h-full flex pt-4 sm:p-0 sm:items-center'>
        <LoginForm />
      </div>
    </main>
  );
}

export default App;
