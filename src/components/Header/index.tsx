export const Header = () => {
  return (
    <header className='h-[60px] sm:fixed top-0 left-0 w-full bg-black
     text-white flex justify-start sm:justify-center items-center text-2xl'
    >Registration
    </header>
  );
};
