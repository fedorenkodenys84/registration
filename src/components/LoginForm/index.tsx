import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import CheckMark from '../../assets/check-mark.svg';
import XMark from '../../assets/x-mark.svg';

interface IValues {
  email: string;
  password: string;
}

interface IPasswordCriteria {
  value: "" | RegExpMatchArray | null | boolean;
  children: React.ReactNode;
}

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .email("Wrong email format")
    .required("The email field is required"),
  password: Yup.string()
    .required("Wrong password format")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
    ),
});

const PasswordCriteria = ({ value, children }: IPasswordCriteria) => (
  <p className="text-md text-black flex">
    {value
      ? <img height={20} width={20} src={CheckMark} alt="check-mark" />
      : <img height={20} width={20} src={XMark} alt="x-mark" />}
    {children}
  </p>
);

export const LoginForm = () => {
  return (
    <div className="w-[600px] mx-auto px-4 sm:p-0">
      <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={validationSchema}
        onSubmit={(values: IValues) => {
          alert(`email: ${values.email} \n password: ${values.password}`);
        }}
      >
        {({ isSubmitting, values }) => (
          <Form className="bg-white grid grid-cols-12 gap-4 border-2 border-black px-4 py-8">
            <div className="col-span-12 sm:col-span-6">
              <label
                className="block text-lg mb-4"
                htmlFor="email"
              >
                Email
              </label>
              <Field
                className="border-2 border-black p-4 focus:outline-none text-lg mb-4 w-full"
                type="email"
                name="email"
                id="email"
                placeholder="Enter email"
              />
              <ErrorMessage
                name="email"
                component="div"
                className="text-red-500 text-xs italic"
              />
            </div>
            <div className="col-span-12 sm:col-span-6">
              <label
                className="block text-lg mb-4"
                htmlFor="password"
              >
                Password
              </label>
              <Field
                className="border-2 border-black p-4 focus:outline-none text-lg mb-4 w-full"
                type="password"
                name="password"
                id="password"
                placeholder="Enter password"
              />
              <PasswordCriteria
                value={values.password && values.password.length >= 8}
              >
                8+ characters
              </PasswordCriteria>
              <PasswordCriteria
                value={
                  values.password &&
                  values.password.match(/[A-Z]/)
                }
              >
                uppercase letter
              </PasswordCriteria>
              <PasswordCriteria
                value={
                  values.password &&
                  values.password.match(/[a-z]/)
                }
              >
                lowercase letter
              </PasswordCriteria>
              <PasswordCriteria
                value={values.password && values.password.match(/\d/)}
              >
                number
              </PasswordCriteria>
              <PasswordCriteria
                value={values.password && values.password.match(/[@$!%*?&]/)}
              >
                special character
              </PasswordCriteria>
              <div className="flex items-center justify-between pt-8">
                <button
                  className="bg-black w-full py-4 text-white focus:outline-none focus:shadow-outline"
                  type="submit"
                  disabled={isSubmitting}
                >
                  Submit
                </button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>);
};
